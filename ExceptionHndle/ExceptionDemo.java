package exception;
import java.util.Scanner;
public class ExceptionDemo {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("enter 1st num :");
		int n1=s.nextInt();
		System.out.println("enter 2nd num :");
		int n2=s.nextInt();
		ExceptionDemo e=new ExceptionDemo();
		e.div(n1,n2);
	}

	public void div(int n1,int n2) {
		try {
			//0 divide
		System.out.println(n1/n2);
		
		// array negative index 
		int[] arr=new int[n1];
		
		// array index out of bound 
		for(int i=0;i<30;i++) {
			System.out.println(arr[i]);
		}
		}
		catch(ArithmeticException ar) {
		 System.out.println("infinity ");
		}
		catch(NegativeArraySizeException ne) {
			System.out.println("negative index");
		}
		catch(ArrayIndexOutOfBoundsException aoi) {
			System.out.println("out of index");
		}

}
}
