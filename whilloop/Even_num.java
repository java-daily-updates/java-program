package whilloop;

public class Even_num {

	public static void main(String[] args) {
		int n=1;
		
		while(n<=100) {
			
			if(n%2==0) {
				System.out.println(n);
			}
			n=n+1;
		}
				
	}

}
