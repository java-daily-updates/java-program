package typecast;

public class Employee extends Student{

	public static void main(String[] args) {
		
		Employee emp=new Employee();
		Student s=emp;//up casting
		s.study();
		
		Student ss=new Employee();
		Employee e=(Employee)ss;
		e.doproject();
	}
	public void doproject() {
		System.out.println("do project ...");
	}
}
