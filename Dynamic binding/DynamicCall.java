public class DynamicCall extends Dynamic1
{
public static void main(String[] args)
{
	DynamicCall c=new DynamicCall();//static binding
	Dynamic1 d= new DynamicCall();//dynamic binding
	c.study();
	d.study();
	c.work();
	d.work();
	
}
public void study()
{
  System.out.println("2 nd methode ....");
  super.study();
}
}