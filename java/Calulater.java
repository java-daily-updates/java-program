public class Calculator
{
	public static void main(String[] args)//main method(pre defined method)
	{
		//local variables
Calculator c=new Calculator();
		int r =c.add(); //method calling statement(add-user defined method)
		//System.out.println(c.add());
		//method overloading-compile time polymorphish 
		//same method name with diff. no.of arguments or with diff. types og arrguments
		c.divide();
		c.divide(r);
		c.divide(r,10);
		c.divide(10.5f);
		}
	public void divide(float fl)
	{
		System.out.println(fl);
	}
	
	public void divide()
	{
		//System.out.println("abc");
	}
	
	
	
	public void divide(int r)
	{
		System.out.println(r/2);
	}
	
	public void divide(int i,int j)
	{
		System.out.println(i/j);
	}
	
	//method defention
	public int add()//method signature
	{    //method body
	// int no1 = 10;
	// int no2 = 20;
	// int result = no1+no2; //if a variable is called manytimes it can have types.
		return 10+20;
	}
}
