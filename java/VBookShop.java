public class VBookShop
{
 public VBookShop(String bookName,String author,int price)
 {
  System.out.println("Are you constructor"); 
  this.bookName=bookName;
  this.author = author;
  this.price = price;   
 }
//Return dataType not mentioned
//Should be same in class name
//Called automatically when object is created 
//Initializing object specific values  
 String bookName;
 String author;
 int price;
public static void main(String[] args)
{
 int no = 10;
 VBookShop book1 = new VBookShop("abc","author1",45);
 //book1.bookName="abc";
 //book1.author="author1";
 //book1.price = 45;
 book1.sell();
 
 VBookShop book2 = new VBookShop("bcd","author2",50);
 //book2.bookName="bcd";
 //book2.author="author2";
 //book2.price = 50;
 book2.sell();
}
public void sell()
{
 System.out.println("BookName : "+bookName);
 System.out.println("Author : "+author);
 System.out.println("Price : "+price);
 this.buy();
 //System.out.println("no : "+no);
}
public void buy()
{
  System.out.println("buy methode   ...... ");
}
}